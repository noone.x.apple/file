#import <UIKit/UIKit.h>
#import "XADBase.h"
#import "XADDefines.h"

@class XADAppRecommendAd;

@protocol XADAppRecommendAdDelegate <NSObject>

@optional

/**
 *  广告预加载成功回调
 *  详解:当接收服务器返回的广告数据成功且预加载后调用该函数
 */
- (void)appRecommendAdSuccessToLoadAd:(XADAppRecommendAd *)appRecommendAd;

/**
 *  广告预加载失败回调
 *  详解:当接收服务器返回的广告数据失败后调用该函数
 */
- (void)appRecommendAdFailToLoadAd:(XADAppRecommendAd *)appRecommendAd error:(NSError *)error;

/**
 *  插屏广告展示结束回调
 *  详解: 插屏广告展示结束回调该函数
 */
- (void)appRecommendAdDidDismissScreen:(XADAppRecommendAd *)appRecommendAd;

/**
 *  插屏广告点击回调
 */
- (void)appRecommendAdClicked:(XADAppRecommendAd *)appRecommendAd;

@end

@interface XADAppRecommendAd : NSObject<XADBase>

/**
 *  插屏广告预加载是否完成
 */
@property (nonatomic, assign) BOOL isReady;

/**
 *  委托对象
 */
@property (nonatomic, weak) id<XADAppRecommendAdDelegate> delegate;

/**
 *  构造方法
 *  详解：appId - 媒体 ID
 *       placementId - 广告位 ID
 */
- (instancetype)initWithPosId:(NSString *)posId
               viewController:(UIViewController *)viewController;


- (void)loadAd;

/**
 *  广告展示方法
 *  详解：[必选]发起展示广告请求, 必须传入用于显示插播广告的UIViewController
 */
- (BOOL)presentFromRootViewController:(UIViewController *)rootViewController;

@end
