#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, XAdOperationType) {
    XAdOperationNone = 0,
    XAdOperationWebView = 1,
    XAdOperationBrowser = 2,
    XAdOperationDownload = 3,
    XAdOperationDeepLink = 4,
    XAdOperationCustom = 5,
    XAdOperationAppStore = 3,//根据服务端代码修改
};

@interface XAdOperation : NSObject<NSCoding>
@property (nonatomic, assign) XAdOperationType type;
@property (nonatomic, copy) NSString* value;
/**新增值仅当 type == TYPE_CUSTOM 时有值 (可取值如:  1- 漫画,  2- 专题),*/
@property (nonatomic, assign) int valueType;
@end

NS_ASSUME_NONNULL_END
