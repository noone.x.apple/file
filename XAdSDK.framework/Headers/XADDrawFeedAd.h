#import "XADBase.h"
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class XADDrawFeedAd;
@class XADDrawFeedAdView;

@protocol XADDrawFeedAdDelegate <NSObject>
@optional
/**
 * 拉取原生模板广告成功
 */
- (void)drawFeedAdSuccessToLoad:(XADDrawFeedAd *)drawFeedAd views:(NSArray<__kindof XADDrawFeedAdView *> *)views;

/**
 * 拉取原生模板广告失败
 */
- (void)drawFeedAdFailToLoad:(XADDrawFeedAd *)drawFeedAd error:(NSError *)error;

/**
 * 原生模板广告渲染成功, 此时的 drawFeedAdView.size.height 根据 size.width 完成了动态更新。
 */
- (void)drawFeedAdViewRenderSuccess:(XADDrawFeedAdView *)drawFeedAdView;

/**
 * 原生模板广告渲染失败
 */
- (void)drawFeedAdViewRenderFail:(XADDrawFeedAdView *)drawFeedAdView;

/**
 * 原生模板广告曝光回调
 */
- (void)drawFeedAdViewExposure:(XADDrawFeedAdView *)drawFeedAdView;

/**
 * 原生模板广告点击回调
 */
- (void)drawFeedAdViewClicked:(XADDrawFeedAdView *)drawFeedAdView;


@end

@interface XADDrawFeedAd : NSObject<XADBase>
/**
 *  委托 [可选]
 */
@property (nonatomic, weak) id<XADDrawFeedAdDelegate> delegate;

@property (nonatomic, readonly) NSString *posId;

/**
 *  构造方法
 *  详解：
 *       posId - 广告位 ID
 *       adSize - 广告展示的宽高
 */
- (instancetype)initWithPosId:(NSString *)posId adSize:(CGSize)size;


- (void) loadAd:(NSInteger)count viewController:(UIViewController*) viewController;


- (void) readyToShowInTableView:(UITableView*) tableView;
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView;

@end

NS_ASSUME_NONNULL_END
