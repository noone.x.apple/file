#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XAdMeta : NSObject
@property (nonatomic, copy) NSString* adPosId;
@property (nonatomic, assign) NSInteger adType;
@property (nonatomic, assign) NSInteger adCount;
@property (nonatomic, assign) NSInteger width;
@property (nonatomic, assign) NSInteger height;

- (instancetype)initWithPosId:(NSString*) posId type:(NSInteger)type width:(NSInteger) w height:(NSInteger)h;

@end

NS_ASSUME_NONNULL_END
