#import "XADBase.h"
#import <UIKit/UIKit.h>
#import "XADDefines.h"

@class XADNativeExpressAdView;
@class XADNativeExpressAd;

@protocol XADNativeExpressAdDelegete <NSObject>

@optional
/**
 * 拉取原生模板广告成功
 */
- (void)nativeExpressAdSuccessToLoad:(XADNativeExpressAd *)nativeExpressAd views:(NSArray<__kindof XADNativeExpressAdView *> *)views;

/**
 * 拉取原生模板广告失败
 */
- (void)nativeExpressAdFailToLoad:(XADNativeExpressAd *)nativeExpressAd error:(NSError *)error;

/**
 * 原生模板广告渲染成功, 此时的 nativeExpressAdView.size.height 根据 size.width 完成了动态更新。
 */
- (void)nativeExpressAdViewRenderSuccess:(XADNativeExpressAdView *)nativeExpressAdView;

/**
 * 原生模板广告渲染失败
 */
- (void)nativeExpressAdViewRenderFail:(XADNativeExpressAdView *)nativeExpressAdView;

/**
 * 原生模板广告曝光回调
 */
- (void)nativeExpressAdViewExposure:(XADNativeExpressAdView *)nativeExpressAdView;

/**
 * 原生模板广告点击回调
 */
- (void)nativeExpressAdViewClicked:(XADNativeExpressAdView *)nativeExpressAdView;

/**
 * 原生模板视频广告 player 播放状态更新回调
 */
- (void)nativeExpressAdView:(XADNativeExpressAdView *)nativeExpressAdView
        playerStatusChanged:(XADMediaPlayerStatus)status;

@end

@interface XADNativeExpressAd : NSObject<XADBase>

/**
 *  委托对象
 */
@property (nonatomic, weak) id<XADNativeExpressAdDelegete> delegate;


/**
 *  非 WiFi 网络，是否自动播放。默认 NO。loadAd 前设置。
 */

@property (nonatomic, assign) BOOL videoAutoPlayOnWWAN;

/**
 *  自动播放时，是否静音。默认 YES。loadAd 前设置。
 */
@property (nonatomic, assign) BOOL videoMuted;

/**
 *  视频详情页播放时是否静音。默认NO。loadAd 前设置。
 */
@property (nonatomic, assign) BOOL detailPageVideoMuted;

/**
 请求视频的时长下限，视频时长有效值范围为[5,60]。
 以下两种情况会使用系统默认的最小值设置，1:不设置  2:minVideoDuration大于maxVideoDuration
 */
@property (nonatomic) NSInteger minVideoDuration;

/**
 请求视频的时长上限，视频时长有效值范围为[5,60]。
 */
@property (nonatomic) NSInteger maxVideoDuration;

@property (nonatomic, readonly) NSString *posId;

/**
 *  构造方法
 *  详解：
 *       posId - 广告位 ID
 *       adSize - 广告展示的宽高
 */
- (instancetype)initWithPosId:(NSString *)posId adSize:(CGSize)size;

- (void) loadAd:(NSInteger)count inViewController:(UIViewController*) controller;

- (void) readyToShowInTableView:(UITableView*) tableView;
- (void) tableView:(UITableView*) tableView didEndDisplayingCell:(UITableViewCell*) cell at:(NSIndexPath*) indexPath;
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView;
@end
