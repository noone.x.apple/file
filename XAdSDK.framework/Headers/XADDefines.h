#ifndef XADDefines_h
#define XADDefines_h

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

static NSString * const XAdErrorDomain = @"com.xadsdk.error.domain";

/**
 *  视频播放器状态
 *
 *  播放器只可能处于以下状态中的一种
 *
 */
typedef NS_ENUM(NSUInteger, XADMediaPlayerStatus) {
    XADMediaPlayerStatusInitial = 0,         // 初始状态
    XADMediaPlayerStatusLoading = 1,         // 加载中
    XADMediaPlayerStatusStarted = 2,         // 开始播放
    XADMediaPlayerStatusPaused = 3,          // 用户行为导致暂停
    XADMediaPlayerStatusStoped = 4,          // 播放停止
    XADMediaPlayerStatusError = 5,           // 播放出错
};
/**
 日志级别
 */
typedef NS_ENUM(NSInteger, XAdLogLevel) {
    XAdLogLevelAll,
    XAdLogLevelVerbose,
    XAdLogLevelDebug,
    XAdLogLevelInfo,
    XAdLogLevelWarn,
    XAdLogLevelNone,
};

typedef NS_ENUM(NSInteger, XADGravity) {
    XAdGravityLeftTop,
    XAdGravityLeftBottom,
    XAdGravityRightTop,
    XAdGravityRightBottom,
};

typedef NS_ENUM(NSInteger, XAdType) {
    
    /**
     * 开屏广告
     */
    XAdTypeSplash = 1,
    
    /**
     Banner广告
     */
    XAdTypeBanner = 2,
    
    /**
     * 轮播广告
     */
    XAdTypeSlideShow = 3,
    
    /**
     * 贴片图广告
     */
    XAdTypeImage = 4,
    
    /**
     * 信息流广告
     */
    XAdTypeNativeExpress = 5,
    /**
     * 插屏广告
     */
    XAdTypeInterstitial = 6,
    /**
     * App推荐广告
     */
    XAdTypeRecommandApp = 7,
    /**
     * 悬浮广告
     */
    XAdTypeFloat = 8,
    
    /**
     * Draw视频流广告
     */
    XAdTypeDrawFeed = 9,
    
    /**
    * 默认，广告数据的处理由接入APP自己实现
    * 自定义广告类型，展示效果由接入APP自己实现
    */
    XAdTypeCustomDefault = 99,
};


static inline BOOL isIPhoneXSeries() {
    if (@available(iOS 11.0, *)) {
        UIWindow *mainWindow = [[[UIApplication sharedApplication] delegate] window];
        if (mainWindow.safeAreaInsets.bottom > 0.0) {
            return YES;
        }
    }
    return NO;
}
#define UIColorFromHex(s) [UIColor colorWithRed:(((s & 0xFF0000) >> 16))/255.0 green:(((s &0xFF00) >>8))/255.0 blue:((s &0xFF))/255.0 alpha:1.0]


#endif /* XADDefines_h */
