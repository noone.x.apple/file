#import "XHTTPSessionManager.h"

NS_ASSUME_NONNULL_BEGIN
@class EventMeta;
@class XAdMeta;
@class XAdObject;

@interface XApiSessionManager : XHTTPSessionManager
+(instancetype) shareInstance;

- (NSURLSessionDataTask*) getAdWithMeta:(XAdMeta*) meta
               success:(void (^)(NSURLSessionDataTask *task, NSArray<XAdObject*>* __nullable adObject))success
               failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask*) sendEvent:(EventMeta*) event;
@end

NS_ASSUME_NONNULL_END
