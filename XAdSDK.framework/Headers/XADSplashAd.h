#import <UIKit/UIKit.h>
#import "XADBase.h"
#import "XADDefines.h"


@class XADSplashAd;

@protocol XADSplashAdDelegate <NSObject>

@optional

/**
 *  开屏广告素材加载成功
 */
- (void)splashAdDidLoad:(XADSplashAd *)splashAd;

/**
 *  开屏广告展示失败
 */
- (void)splashAdFailToPresent:(XADSplashAd *)splashAd withError:(NSError *)error;

/**
 *  开屏广告曝光回调
 */
- (void)splashAdExposured:(XADSplashAd *)splashAd;

/**
 *  开屏广告点击回调
 */
- (void)splashAdClicked:(XADSplashAd *)splashAd;

/**
 *  开屏广告关闭回调
 */
- (void)splashAdClosed:(XADSplashAd *)splashAd;

@end

@interface XADSplashAd : NSObject<XADBase>

@property (nonatomic, weak) id<XADSplashAdDelegate> delegate;

/**
 *  拉取广告超时时间，默认为3秒
 *  详解：拉取广告超时时间，开发者调用loadAd方法以后会立即展示backgroundImage，然后在该超时时间内，如果广告拉
 *  取成功，则立马展示开屏广告，否则放弃此次广告展示机会。
 */
@property (nonatomic, assign) CGFloat fetchDelay;

/**
 *  开屏广告的背景图片
 *  可以设置背景图片作为开屏加载时的默认背景
 */
@property (nonatomic, strong) UIImage *backgroundImage;

/**
 *  开屏广告的背景色
 *  可以设置开屏图片来作为开屏加载时的默认图片
 */
@property (nonatomic, copy) UIColor *backgroundColor;

/**
 *  构造方法
 *       posId - 广告位 ID
 */
- (instancetype)initWithPosId:(NSString *)posId;

/**
 *  广告发起请求并展示在Window中, 同时在屏幕底部设置应用自身的Logo页面或是自定义View,skipView是自定义的“跳过”样式
 *  详解：[可选]发起拉取广告请求,并将获取的广告以半屏形式展示在传入的Window的上半部，剩余部分展示传入的bottomView
 *       请注意1.bottomView需设置好宽高，所占的空间不能过大，并保证高度不超过屏幕高度的 25%。2.Splash广告只支持竖屏
 *  skipView
 *  @param window 展示开屏的容器
 *         bottomView 自定义底部View，可以在此View中设置应用Logo
 skipView 自定义”跳过“View.
 */
- (void)loadAdAndShowInWindow:(UIWindow *)window
               withBottomView:(UIView *)bottomView
               viewController:(UIViewController*) viewController;
/**
 预加载闪屏广告接口
 
 @param appId 媒体ID
 @param placementId 广告位ID
 */
+ (void)preloadSplashOrderWithAppId:(NSString *)appId placementId:(NSString *)placementId;

#pragma mark - Parallel method

/**
 * 返回广告是否可展示
 * 对于并行请求，在调用showAdInWindow前时需判断下
 * @return 当广告已经加载完成且未曝光时，为YES，否则为NO
 */
- (BOOL)isAdValid;
//
///**
// *  发起拉取广告请求，只拉取不展示
// *  详解：广告素材及广告图片拉取成功后会回调splashAdDidLoad方法，当拉取失败时会回调splashAdFailToPresent方法
// */
//- (void)loadAd;
//
///**
// *  展示广告，调用此方法前需调用isAdValid方法判断广告素材是否有效
// *  详解：广告展示成功时会回调splashAdSuccessPresentScreen方法，展示失败时会回调splashAdFailToPresent方法
// */
//- (void)showAdInWindow:(UIWindow *)window withBottomView:(UIView *)bottomView skipView:(UIView *)skipView;

@end
