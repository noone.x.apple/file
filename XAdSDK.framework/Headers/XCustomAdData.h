#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class XAdObject;
@class XAdVideoInfo;
@class XAdOperation;
@class XAdDownloadInfo;

/**
 自定义广告数据结构
 注意以下字段中包含了引号的部分，通常对应了后台的同名目录
 请结合参数以及日志以及后台页面对应各个字段内容
 */

@interface XCustomAdData : NSObject<NSCoding>
/**
 广告位Id
 */
@property (nonatomic, copy, readonly) NSString* adPosId;
/**
 当前”广告位“的排期返回的广告Id
 对应到后台”广告资源“->”广告“，条目中的广告位Id
 */
@property (nonatomic, copy, readonly) NSString* adId;

/**
以下字段对应上一条的"广告素材"
 */
@property (nonatomic, copy, readonly) NSString* title;
@property (nonatomic, copy, readonly) NSString* subtitle;
@property (nonatomic, copy, readonly) NSString* imageUrl;
@property (nonatomic, copy, readonly) NSString* nickname;
@property (nonatomic, strong, readonly) XAdVideoInfo* videoInfo;
/**
 当前广告位的排期返回的”广告“中的”广告内容“
 */
@property (nonatomic, strong, readonly) XAdOperation* operation;
/**
 后台：“媒体资源”->"广告位"，对应广告位详情的是否跳过
 */
@property (nonatomic, assign, readonly) BOOL autoSkip;
/**
后台：“媒体资源”->"广告位"，对应广告位详情的间隔时间
*/
@property (nonatomic, assign, readonly) int slideShowDuration;
/**
后台：“媒体资源”->"广告位"，对应广告位详情的倒计时
*/
@property (nonatomic, assign, readonly) int countDown;
/**
 后台：“媒体资源”->"广告位"，对应广告位详情的关闭按钮
 XAdCloseButtonShow = 1,
 XAdCloseButtonHide = 2,
 XAdCloseButtonDelayShow = 3,
 */
@property (nonatomic, assign, readonly) int closeBtnShowType;
/**
后台：“媒体资源”->"广告位"，对应广告位详情的延迟
 */
@property (nonatomic, assign, readonly) int closeBtnShowDelay;
@property (nonatomic, assign, readonly) int weight;
/**
后台：“媒体资源”->"广告位"，对应广告位详情的背景
*/
@property (nonatomic, copy, readonly) NSString* bgColor;
@property (nonatomic, assign, readonly) int needCountDown;
@property (nonatomic, copy, readonly) NSString* authorAvatar;

/**
 * 获取后台“广告内容”中配置的扩展参数
 */
@property (nonatomic, copy, readonly) NSDictionary<NSString*, NSString*> *adContextExtraParam;

/**
 * 通过key值获取后台“广告内容”中配置的扩展参数中对应的值
 */
- (NSString*) adContextExtraParamValueForKey:(NSString*) key;

/**
 * 获取后台“广告位”中配置的扩展参数
 */
@property (nonatomic, copy) NSDictionary<NSString*, NSString*> *adPosExtraParams;

/**
 * 通过key值获取后台“广告位”中配置的扩展参数中对应的值
 */
- (NSString*) adPosExtraParamForKey:(NSString*) key;


- (instancetype)initWithXAdObject:(XAdObject*) obj;

/**上报广告曝光事件*/
- (void) eventExposure;

/**上报广告点击事件*/
- (void) eventClick;

/**上报广告关闭事件*/
- (void) eventClose;
@end

NS_ASSUME_NONNULL_END
