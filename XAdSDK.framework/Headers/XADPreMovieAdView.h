#import "XADBase.h"
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class XADPreMovieAdView;

@protocol XADPreMovieAdViewDelegate <NSObject>
@optional
- (void)preMovieAdViewDidLoad:(XADPreMovieAdView *)preMovieAdView;

- (void)preMovieAdViewFailedToLoad:(XADPreMovieAdView *)preMovieAdView error:(NSError *)error;

- (void)preMovieAdViewWillExpose:(XADPreMovieAdView *)preMovieAdView;

- (void)preMovieAdViewClicked:(XADPreMovieAdView *)preMovieAdView;

- (void)preMovieAdViewWillClose:(XADPreMovieAdView *)preMovieAdView;

@end

@interface XADPreMovieAdView : UIView<XADBase>
/**
 *  委托 [可选]
 */
@property (nonatomic, weak) id<XADPreMovieAdViewDelegate> delegate;

/**
 *  Banner展现和轮播时的动画效果开关，默认打开
 */
@property (nonatomic) BOOL animated;

/**
 *  广告刷新间隔，范围 [30, 120] 秒，默认值 30 秒。设 0 则不刷新。 [可选]
 */
@property (nonatomic) int autoSwitchInterval;
/**
 *  构造方法
 *  详解：appId - 媒体 ID
 *       placementId - 广告位 ID
 *       viewController - 视图控制器
 */
- (instancetype)initWithPosId:(NSString *)posId
               viewController:(UIViewController *)viewController;

/**
 *  拉取并展示广告
 */
- (void)loadAndShowIn:(UIView*) parent;

- (void) dismiss;
@end

NS_ASSUME_NONNULL_END
