#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XAdVideoInfo : NSObject<NSCoding>
@property (nonatomic, copy) NSString* desc;
@property (nonatomic, copy) NSString* url;
@property (nonatomic, copy) NSString* id;
@property (nonatomic, assign) int duration;
@property (nonatomic, assign) long size;
@property (nonatomic, copy) NSString* imageUrl;
@property (nonatomic, copy) NSString* authorId;
@property (nonatomic, copy) NSString* authorAvatar;
@property (nonatomic, copy) NSString* authorNickname;
@property (nonatomic, assign) BOOL isAutoPlay;
@end

NS_ASSUME_NONNULL_END
