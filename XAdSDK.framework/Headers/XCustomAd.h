#import "XADBase.h"

NS_ASSUME_NONNULL_BEGIN

@class XCustomAd;
@class XCustomAdData;

@protocol XCustomAdDelegate<NSObject>
@optional

- (void)customAdViewDidLoad:(XCustomAd*)customAd ads:(NSArray<XCustomAdData*> *) adArray;

- (void)customAdViewFailedToLoad:(XCustomAd *)customAd error:(NSError *)error;

@end

@class UIViewController;

@interface XCustomAd : NSObject<XADBase>

- (instancetype)initWithPosId:(NSString *)posId
               viewController:(UIViewController *)viewController;
/**
 请求广告
 @param count : 最大请求条数
 */
- (void) loadAd:(int) count;

@property(nonatomic, weak) id<XCustomAdDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
