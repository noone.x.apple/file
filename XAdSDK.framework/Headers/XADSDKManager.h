/*!
@header XADSDKManager.h

@brief XAD管理类

 提供了初始化、服务器切换以及debug接口

@author xad
*/

#import <Foundation/Foundation.h>

#include "XADDefines.h"

NS_ASSUME_NONNULL_BEGIN

@interface XADSDKManager : NSObject

/*! @brief SDK版本号*/
@property (nonatomic, copy, readonly, class) NSString *sdkVersion;
/*! @brief appSecret，平台分配*/
@property (nonatomic, copy, readonly, class) NSString *appSecret;
/*! @brief 渠道号*/
@property (nonatomic, copy, readonly, class) NSString *appChannel;
/*! @brief 用户Id*/
@property (nonatomic, copy, readonly, class) NSString *appUserId;

/*!
   SDK初始化
   @param appId : 后台分配
   @param appSecret : 后台分配
   @param channel : channel
   @param userId : userId
   @return 成功返回YES，参数无效或者重复初始化返回NO
*/
+ (BOOL)initWithAppId:(NSString *)appId appSecret:(NSString*) appSecret
              channel:(NSString*) channel userId:(NSString*) userId;

+ (void)setLoglevel:(XAdLogLevel)level;

/*!
 设置调试参数
 @param level : 设置日志等级
 @param debug : 是否是测试服务器
 */
+ (void)setLoglevel:(XAdLogLevel)level debugServer:(BOOL) debug;

/*!
 get appId
 */
+ (const NSString *)appID;

/*!
 debug server
 */
+ (BOOL)debugServer;

@end

NS_ASSUME_NONNULL_END
