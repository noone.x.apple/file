#import "XADBase.h"
#import <UIKit/UIKit.h>
#import "XADDefines.h"

@class XADInterstitial;

@protocol XADInterstitialDelegate <NSObject>

@optional

/**
 *  广告预加载成功回调
 *  详解:当接收服务器返回的广告数据成功且预加载后调用该函数
 */
- (void)interstitialSuccessToLoadAd:(XADInterstitial *)interstitial;

/**
 *  广告预加载失败回调
 *  详解:当接收服务器返回的广告数据失败后调用该函数
 */
- (void)interstitialFailToLoadAd:(XADInterstitial *)interstitial error:(NSError *)error;


/**
 *  插屏广告曝光回调
 */
- (void)interstitialWillExposure:(XADInterstitial *)interstitial;

/**
 *  插屏广告点击回调
 */
- (void)interstitialClicked:(XADInterstitial *)interstitial;

@end

@interface XADInterstitial : NSObject<XADBase>

/**
 *  GPS精准广告定位模式开关,默认Gps关闭
 *  详解：[可选]GPS精准定位模式开关，YES为开启GPS，NO为关闭GPS，建议设为开启，可以获取地理位置信息，提高广告的填充率，增加收益。
 */
@property (nonatomic, assign) BOOL isGpsOn;

/**
 *  插屏广告预加载是否完成
 */
@property (nonatomic, assign) BOOL isReady;

/**
 *  委托对象
 */
@property (nonatomic, weak) id<XADInterstitialDelegate> delegate;

/**
 *  构造方法
 *  详解：appId - 媒体 ID
 *       placementId - 广告位 ID
 */
- (instancetype)initWithPosId:(NSString *)posId
               viewController:(UIViewController *)viewController;

/**
 *  广告发起请求方法
 *  详解：[必选]发起拉取广告请求
 */
- (void)loadAd;

/**
 *  广告展示方法
 *  详解：[必选]发起展示广告请求, 必须传入用于显示插播广告的UIViewController
 */
- (BOOL)presentFromRootViewController:(UIViewController *)rootViewController;

@end
