#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XAdDownloadInfo : NSObject<NSCoding>
@property (nonatomic, copy) NSString* appName;
@property (nonatomic, copy) NSString* packageName;
@property (nonatomic, assign) long apkSize;
@property (nonatomic, copy) NSString* md5;
@property (nonatomic, copy) NSString* iconUrl;
@property (nonatomic, copy) NSString* downloadUrl;
@property (nonatomic, copy) NSString* desc;
@end

NS_ASSUME_NONNULL_END
