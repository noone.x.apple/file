#import <UIKit/UIKit.h>
#import "XADBase.h"

NS_ASSUME_NONNULL_BEGIN

@class XADBannerView;

@protocol XADBannerViewDelegate <NSObject>
@optional
- (void)bannerViewDidLoad:(XADBannerView *)bannerView;

- (void)bannerViewFailedToLoad:(XADBannerView *)bannerView error:(NSError *)error;

- (void)bannerViewWillExpose:(XADBannerView *)bannerView;

- (void)bannerViewClicked:(XADBannerView *)bannerView;

- (void)bannerViewWillClose:(XADBannerView *)bannerView;

@end

@interface XADBannerView : UIView<XADBase>
/**
 *  委托 [可选]
 */
@property (nonatomic, weak) id<XADBannerViewDelegate> delegate;

/**
 *  Banner展现和轮播时的动画效果开关，默认打开
 */
@property (nonatomic) BOOL animated;

@property (nonatomic) BOOL showCloseButton;
/**
 *  构造方法
 *  详解：appId - 媒体 ID
 *       placementId - 广告位 ID
 *       viewController - 视图控制器
 */
- (instancetype)initWithPosId:(NSString *)posId
               viewController:(UIViewController *)viewController;

/**
 *  构造方法
 *  详解：frame - banner 展示的位置和大小
 *       appId - 媒体 ID
 *       placementId - 广告位 ID
 *       viewController - 视图控制器
 */
- (instancetype)initWithFrame:(CGRect)frame
                        posId:(NSString *)posId
               viewController:(UIViewController *)viewController;

/**
 *  拉取并展示广告
 */
- (void)loadAdAndShow;

@end

NS_ASSUME_NONNULL_END
