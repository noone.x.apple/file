#import "XADBase.h"
#import <UIKit/UIKit.h>
#import "XADDefines.h"

static NSInteger kFloatAdWidth = 120;
static NSInteger kFloatAdHeight = 120;


NS_ASSUME_NONNULL_BEGIN
@class XADFloatAdView;

@protocol XADFloatAdViewDelegate <NSObject>
@optional
- (void)floatAdViewDidLoad:(XADFloatAdView *)floatAdView;

- (void)floatAdViewFailedToLoad:(XADFloatAdView *)floatAdView error:(NSError *)error;

- (void)floatAdViewWillExpose:(XADFloatAdView *)floatAdView;

- (void)floatAdViewClicked:(XADFloatAdView *)floatAdView;

- (void)floatAdViewWillClose:(XADFloatAdView *)floatAdView;

@end

@interface XADFloatAdView : UIView<XADBase>
@property (readonly, assign) CGSize adSize;
@property (nonatomic, weak) id<XADFloatAdViewDelegate> delegate;
@property (nonatomic, strong) UIImage* closeButtonImage;
/**
 *  构造方法
 *  详解：appId - 媒体 ID
 *       placementId - 广告位 ID
 *       viewController - 视图控制器
 */
- (instancetype)initWithPosId:(NSString *)posId
                       adSize:(CGSize) adSize
               viewController:(UIViewController *)viewController;

- (instancetype)initWithPosId:(NSString *)posId
               viewController:(UIViewController *)viewController;

/**
 *  拉取并展示广告
 */
- (void)loadAndShowIn:(UIView*) parent gravity:(XADGravity) gravity;

- (void) dismiss;
@end

NS_ASSUME_NONNULL_END
