#import "XADBase.h"
#import <UIKit/UIKit.h>


NS_ASSUME_NONNULL_BEGIN

@class XADSlideShowAd;

@protocol XADSlideShowAdDelegate <NSObject>
@optional

- (void) slideShowAdDidLoad:(XADSlideShowAd *)slideShowAd;

/**
 *  请求广告条数据失败后调用
 *  当接收服务器返回的广告数据失败后调用该函数
 */
- (void)slideShowAdFailedToLoad:(XADSlideShowAd *)slideShowAd error:(NSError *)error;

/**
 *  banner2.0曝光回调
 */
- (void)slideShowAdWillExpose:(XADSlideShowAd *)slideShowAd;

/**
 *  banner2.0点击回调
 */
- (void)slideShowAdClicked:(XADSlideShowAd *)slideShowAd;

/**
 *  banner2.0被用户关闭时调用
 */
- (void)slideShowAdClose:(XADSlideShowAd *)slideShowAd;

@end

@interface XADSlideShowAd : UIView<XADBase>
/**
 *  委托 [可选]
 */
@property (nonatomic, weak) id<XADSlideShowAdDelegate> delegate;

/**
 *  广告刷新间隔，范围 [3,10] 秒，默认值 3 秒
 */
@property (nonatomic) int autoSwitchInterval;
/**
 *  构造方法
 *  详解：appId - 媒体 ID
 *       placementId - 广告位 ID
 *       viewController - 视图控制器
 */
- (instancetype)initWithPosId:(NSString *)posId
               viewController:(UIViewController *)viewController;

/**
 *  构造方法
 *  详解：frame - banner 展示的位置和大小
 *       appId - 媒体 ID
 *       placementId - 广告位 ID
 *       viewController - 视图控制器
 */
- (instancetype)initWithFrame:(CGRect)frame
                        posId:(NSString *)posId
               viewController:(UIViewController *)viewController;

/**
 *  拉取并展示广告
 */
- (void)loadAdAndShow:(NSInteger) adCount;

@end

NS_ASSUME_NONNULL_END
