#ifndef XADBase_h
#define XADBase_h
#import <Foundation/Foundation.h>

@protocol XADBase<NSObject>
- (void) cancel;
@end
#endif /* XADBase_h */
